import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="cowincli",
    version="0.0.1",
    author="BlackSmithOP",
    author_email="angstycoder101@gmail.com",
    description="CLI app to find vaccination centers",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/blacksmithop/cowincli",
    project_urls={
        "Bug Tracker": "https://gitlab.com/blacksmithop/cowincli/-/issues",
    },
     entry_points='''
        [console_scripts]
        cowincli=cowincli.__main__:main
    ''',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
)