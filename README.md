Usage:

```
usage: CowinCLI [-h] [-pincode PINCODE]

Get information about centers and vaccine availability

optional arguments:
  -h, --help        show this help message and exit
  -pincode PINCODE  Pincode of the region
```

